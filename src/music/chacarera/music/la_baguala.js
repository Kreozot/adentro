// Chacarera simple 6
module.exports = {
	id: 'la_baguala',
	title: 'La Baguala (6)',
	file: require('./la_baguala.mp3'),
	schema: require('./la_baguala.yaml'),
	schemeMods: {
		vuelta: {times: 6},
		vuelta2: {times: 6},
		vuelta_2: {times: 6},
		vuelta2_2: {times: 6}
	}
};
