// Chacarera doble 6
module.exports = {
	id: 'chacarera_doble_6_anorazas',
	title: 'Anorazas (6)',
	file: require('./anorazas.mp3'),
	schema: require('./anorazas.yaml'),
	schemeMods: {
		vuelta: {times: 6},
		vuelta2: {times: 6},
		vuelta_2: {times: 6},
		vuelta2_2: {times: 6}
	}
};
