module.exports = {
	name: 'Remedio',
	scheme: require('./scheme.yaml'),
	music: [require('./music/remedio_norteno')],
	animation: require('animationClasses/RemedioAnimation').default,
	zapateo: true
};
