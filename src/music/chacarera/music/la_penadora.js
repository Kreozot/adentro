// Chacarera simple 6
module.exports = {
	id: 'la_penadora',
	title: 'La Penadora (6)',
	file: require('./la_penadora.mp3'),
	schema: require('./la_penadora.yaml'),
	schemeMods: {
		vuelta: {times: 6},
		vuelta2: {times: 6},
		vuelta_2: {times: 6},
		vuelta2_2: {times: 6}
	}
};
